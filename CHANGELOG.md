## Version 8
* Support for tablets
* Hiding system UI on countdown screen

## Android version 3
* Added support for Polish
* Alarm sound is played using Alarm settings from the phone
* App icon changed, UI changes
* About application is back, direct message author from application